<?php

namespace App\Providers;

use Auth\Infrastructure\Interfaces\Providers\Services\AuthServiceProviderInterface;
use Customer\Application\Interfaces\Services\Customers\CustomerServiceInterface;
use Customer\Application\Interfaces\Services\Documents\DocumentTypeServiceInterface;
use Customer\Application\Interfaces\Services\Documents\PersonTypeServiceInterface;
use Customer\Application\Interfaces\Services\Location\CityServiceInterface;
use Customer\Application\Interfaces\Services\Location\StateServiceInterface;
use Customer\Application\Services\Customers\CustomerService;
use Customer\Application\Services\Documents\DocumentTypeService;
use Customer\Application\Services\Documents\PersonTypeService;
use Customer\Application\Services\Location\CityService;
use Customer\Application\Services\Location\StateService;
use Customer\Infrastructure\Interfaces\Repositories\Customers\CustomerRepositoryInterface;
use Customer\Infrastructure\Interfaces\Repositories\Documents\DocumentTypeRepositoryInterface;
use Customer\Infrastructure\Interfaces\Repositories\Documents\PersonTypeRepositoryInterface;
use Customer\Infrastructure\Interfaces\Repositories\Location\CityRepositoryInterface;
use Customer\Infrastructure\Interfaces\Repositories\Location\StateRepositoryInterface;
use Customer\Infrastructure\Repositories\Customers\CustomerRepository;
use Customer\Infrastructure\Repositories\Documents\DocumentTypeRepository;
use Customer\Infrastructure\Repositories\Documents\PersonTypeRepository;
use Customer\Infrastructure\Repositories\Location\CityRepository;
use Customer\Infrastructure\Repositories\Location\StateRepository;
use Illuminate\Support\ServiceProvider;
use Auth\Infrastructure\Providers\Services\AuthServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        // Providers
        $this->app->bind(AuthServiceProviderInterface::class, AuthServiceProvider::class);

        // Services
        $this->app->bind(CustomerServiceInterface::class, CustomerService::class);
        $this->app->bind(DocumentTypeServiceInterface::class, DocumentTypeService::class);
        $this->app->bind(PersonTypeServiceInterface::class, PersonTypeService::class);
        $this->app->bind(StateServiceInterface::class, StateService::class);
        $this->app->bind(CityServiceInterface::class, CityService::class);

        // Repositories
        $this->app->bind(CustomerRepositoryInterface::class, CustomerRepository::class);
        $this->app->bind(DocumentTypeRepositoryInterface::class, DocumentTypeRepository::class);
        $this->app->bind(PersonTypeRepositoryInterface::class, PersonTypeRepository::class);
        $this->app->bind(StateRepositoryInterface::class, StateRepository::class);
        $this->app->bind(CityRepositoryInterface::class, CityRepository::class);
    }
}
