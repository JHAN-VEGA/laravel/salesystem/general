<?php

use Illuminate\Support\Facades\Route;
use Customer\Infrastructure\Controllers\Customers\CustomerController;
use Customer\Infrastructure\Controllers\Documents\DocumentTypeController;
use Customer\Infrastructure\Controllers\Documents\PersonTypeController;
use Customer\Infrastructure\Controllers\Location\CityController;
use Customer\Infrastructure\Controllers\Location\StateController;

Route::prefix('customers')->group(function () {
    Route::get('paginate', [CustomerController::class, 'paginate'])->name('customers.paginate');
    Route::get('search', [CustomerController::class, 'search'])->name('customers.search');
    Route::post('store', [CustomerController::class, 'store'])->name('customers.store');
    Route::get('{id}/show', [CustomerController::class, 'show'])->name('customers.show');
    Route::post('{id}/update', [CustomerController::class, 'update'])->name('customers.update');
});

Route::prefix('document-types')->group(function () {
    Route::get('get-all', [DocumentTypeController::class, 'getAll'])->name('documentTypes.getAll');
});

Route::prefix('person-types')->group(function () {
    Route::get('get-all', [PersonTypeController::class, 'getAll'])->name('personTypes.getAll');
});

Route::prefix('states')->group(function () {
    Route::get('search', [StateController::class, 'search'])->name('states.search');
});

Route::prefix('cities')->group(function () {
    Route::get('search-by-state', [CityController::class, 'searchByStateId'])->name('cities.searchByStateId');
    Route::get('search', [CityController::class, 'search'])->name('cities.search');
});
