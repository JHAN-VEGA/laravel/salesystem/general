<?php

namespace Auth\Application\Mocks\Services;

use Auth\Application\Interfaces\AuthServiceInterface;

class AuthServiceMock
{
    public function generateLoginUserFromTokenWorking()
    {
        $mock = \Mockery::mock(AuthServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('loginUserFromToken')
            ->once()
            ->andReturnSelf();

        return $mock;
    }
}
