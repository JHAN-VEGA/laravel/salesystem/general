<?php

namespace Auth\Infrastructure\Mocks\Providers\Services;

use Auth\Infrastructure\Interfaces\Providers\Services\AuthServiceProviderInterface;

class AuthServiceProviderMock
{
    public function generateGetUserByTokenWorking()
    {
        $mock = \Mockery::mock(AuthServiceProviderInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('getUserByToken')
            ->once()
            ->andReturnUsing(function () {
                return [
                    'id' => 1,
                    'name' => 'admin',
                    'email' => 'admin@admin'
                ];
            });

        return $mock;
    }
}
