<?php

namespace Customer\Application\Mappers\Customer;

use Customer\Application\Mappers\BaseMapper;
use Customer\Domain\Dto\BaseDto;
use Customer\Domain\Dto\Customer\CustomerNewDto;
use Illuminate\Http\Request;

class CustomerNewDtoMapper extends BaseMapper
{
    protected function getNewDto(): CustomerNewDto
    {
        return new CustomerNewDto;
    }

    /**
     * @param Request $request
     * @return CustomerNewDto
     */
    public function createFromRequest(Request $request):CustomerNewDto
    {
        $dto = $this->getNewDto();
        $dto->name = $request->get('name');
        $dto->documentNumber = $request->get('document_number');
        $dto->phone = $request->get('phone');
        $dto->email = $request->get('email');
        $dto->address = $request->get('address');
        $dto->stateId = $request->get('state_id');
        $dto->cityId = $request->get('city_id');
        $dto->documentTypeId = $request->get('document_type_id');
        return $dto;
    }

}
