<?php

namespace Customer\Application\Mappers\Customer;

use Customer\Application\Mappers\BaseMapper;
use Customer\Domain\Dto\BaseDto;
use Customer\Domain\Dto\Customer\CustomerUpdateDto;
use Illuminate\Http\Request;

class CustomerUpdateDtoMapper extends BaseMapper
{
    protected function getNewDto(): CustomerUpdateDto
    {
        return new CustomerUpdateDto;
    }

    public function updateFromRequest(Request $request, int $id):CustomerUpdateDto
    {
        $dto = $this->getNewDto();
        $dto->id = $id;
        $dto->name = $request->get('name');
        $dto->documentNumber = $request->get('document_number');
        $dto->phone = $request->get('phone');
        $dto->email = $request->get('email');
        $dto->address = $request->get('address');
        $dto->stateId = $request->get('state_id');
        $dto->cityId = $request->get('city_id');
        $dto->documentTypeId = $request->get('document_type_id');
        return $dto;
    }
}
