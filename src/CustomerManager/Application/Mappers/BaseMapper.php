<?php

namespace Customer\Application\Mappers;

use Customer\Domain\Dto\BaseDto;

abstract class BaseMapper
{
    /**
     * @var BaseDto
     */
    protected BaseDto $dto;

    /**
     * @return BaseDto
     */
    abstract protected function getNewDto():BaseDto;
}
