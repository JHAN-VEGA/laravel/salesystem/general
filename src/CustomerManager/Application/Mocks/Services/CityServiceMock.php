<?php

namespace Customer\Application\Mocks\Services;

use Customer\Application\Interfaces\Services\Location\CityServiceInterface;

class CityServiceMock
{
    public function generateSearchWorking()
    {
        $mock = \Mockery::mock(CityServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('search')
            ->once()
            ->andReturnUsing(function () {
                $customer = new \stdClass();
                $customer->id = 1;
                $customer->name = 'City name';

                return collect([$customer]);
            });

        return $mock;
    }

    public function generateSearchByStateIdWorking()
    {
        $mock = \Mockery::mock(CityServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('searchByStateId')
            ->once()
            ->andReturnUsing(function () {
                $customer = new \stdClass();
                $customer->id = 1;
                $customer->name = 'City name';

                return collect([$customer]);
            });

        return $mock;
    }
}
