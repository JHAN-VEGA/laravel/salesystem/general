<?php

namespace Customer\Application\Mocks\Services;

use Customer\Application\Interfaces\Services\Customers\CustomerServiceInterface;
use Illuminate\Pagination\LengthAwarePaginator;

class CustomerServiceMock
{
    public function generatePaginateWorking()
    {
        $mock = \Mockery::mock(CustomerServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('paginate')
            ->once()
            ->andReturnUsing(function () {
                $customer = new \stdClass();
                $customer->id = 1;
                $customer->name = 'Customer name';

                return new LengthAwarePaginator([$customer], 1, 1, 1);
            });

        return $mock;
    }

    public function generateSearchWorking()
    {
        $mock = \Mockery::mock(CustomerServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('search')
            ->once()
            ->andReturnUsing(function () {
                $customer = new \stdClass();
                $customer->id = 1;
                $customer->name = 'Customer name';

                return collect([$customer]);
            });

        return $mock;
    }

    public function generateFindByIdWorking()
    {
        $mock = \Mockery::mock(CustomerServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('findById')
            ->once()
            ->andReturnUsing(function () {
                $customer = new \stdClass();
                $customer->id = 1;
                $customer->name = 'Customer name';

                return $customer;
            });

        return $mock;
    }

    public function generateStoreWorking()
    {
        $mock = \Mockery::mock(CustomerServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('store')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('getCustomerId')
            ->once()
            ->andReturn(1);

        return $mock;
    }

    public function generateUpdateWorking()
    {
        $mock = \Mockery::mock(CustomerServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('update')
            ->once()
            ->andReturnSelf();

        return $mock;
    }
}
