<?php

namespace Customer\Application\Mocks\Services;

use Customer\Application\Interfaces\Services\Location\StateServiceInterface;

class StateServiceMock
{
    public function generateSearchWorking()
    {
        $mock = \Mockery::mock(StateServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('search')
            ->once()
            ->andReturnUsing(function () {
                $customer = new \stdClass();
                $customer->id = 1;
                $customer->name = 'State name';

                return collect([$customer]);
            });

        return $mock;
    }
}
