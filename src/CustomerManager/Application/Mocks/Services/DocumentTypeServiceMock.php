<?php

namespace Customer\Application\Mocks\Services;

use Customer\Application\Interfaces\Services\Documents\DocumentTypeServiceInterface;

class DocumentTypeServiceMock
{
    public function generateGetAllWorking()
    {
        $mock = \Mockery::mock(DocumentTypeServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('getAll')
            ->once()
            ->andReturnUsing(function () {
                $documentType = new \stdClass();
                $documentType->id = 1;
                $documentType->name = 'Document type name';

                return collect([$documentType]);
            });

        return $mock;
    }

}
