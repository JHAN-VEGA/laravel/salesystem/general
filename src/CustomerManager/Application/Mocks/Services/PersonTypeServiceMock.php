<?php

namespace Customer\Application\Mocks\Services;

use Customer\Application\Interfaces\Services\Documents\PersonTypeServiceInterface;

class PersonTypeServiceMock
{
    public function generateGetAllWorking()
    {
        $mock = \Mockery::mock(PersonTypeServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('getAll')
            ->once()
            ->andReturnUsing(function () {
                $documentType = new \stdClass();
                $documentType->id = 1;
                $documentType->name = 'Person type name';

                return collect([$documentType]);
            });

        return $mock;
    }

}
