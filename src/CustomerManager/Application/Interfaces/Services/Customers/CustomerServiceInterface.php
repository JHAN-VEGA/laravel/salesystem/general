<?php

namespace Customer\Application\Interfaces\Services\Customers;

use Customer\Domain\Dto\Customer\CustomerNewDto;
use Customer\Domain\Dto\Customer\CustomerUpdateDto;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

interface CustomerServiceInterface
{
    /**
     * @return int
     */
    public function getCustomerId():int;

    public function paginate(Request $request);

    /**
     * @param string $value
     * @return Collection
     */
    public function search(string $value):Collection;

    /**
     * @param int $id
     * @return object|null
     */
    public function findById(int $id):object|null;

    /**
     * @param CustomerNewDto $dto
     * @return self
     */
    public function store(CustomerNewDto $dto):self;

    /**
     * @param CustomerUpdateDto $dto
     * @return self
     */
    public function update(CustomerUpdateDto $dto):self;
}
