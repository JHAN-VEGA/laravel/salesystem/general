<?php

namespace Customer\Application\Interfaces\Services\Location;

use Illuminate\Support\Collection;

interface CityServiceInterface
{
    /**
     * @param int $stateId
     * @param string $value
     * @return Collection
     */
    public function searchByStateId(int $stateId, string $value):Collection;

    /**
     * @param string $value
     * @return Collection
     */
    public function search(string $value):Collection;
}
