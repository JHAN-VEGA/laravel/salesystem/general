<?php

namespace Customer\Application\Interfaces\Services\Location;

use Illuminate\Support\Collection;

interface StateServiceInterface
{
    /**
     * @param string $value
     * @return Collection
     */
    public function search(string $value):Collection;
}
