<?php

namespace Customer\Application\Interfaces\Services\Documents;

use Illuminate\Support\Collection;

interface DocumentTypeServiceInterface
{
    /**
     * @return Collection
     */
    public function getAll():Collection;
}
