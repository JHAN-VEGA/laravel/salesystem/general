<?php

namespace Customer\Application\Interfaces\Services\Documents;

use Illuminate\Support\Collection;

interface PersonTypeServiceInterface
{
    /**
     * @return Collection
     */
    public function getAll():Collection;
}
