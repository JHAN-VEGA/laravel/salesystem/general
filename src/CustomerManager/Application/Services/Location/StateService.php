<?php

namespace Customer\Application\Services\Location;

use Customer\Application\Interfaces\Services\Location\StateServiceInterface;
use Customer\Infrastructure\Interfaces\Repositories\Location\StateRepositoryInterface;
use Illuminate\Support\Collection;

class StateService implements StateServiceInterface
{
    /**
     * @var StateRepositoryInterface
     */
    private StateRepositoryInterface $stateRepo;

    /**
     * @param StateRepositoryInterface $stateRepo
     */
    public function __construct(
        StateRepositoryInterface $stateRepo
    )
    {
        $this->stateRepo = $stateRepo;
    }

    /**
     * @param string $value
     * @return Collection
     */
    public function search(string $value): Collection
    {
        return $this->stateRepo
            ->setUser(auth()->user())
            ->search($value);
    }


}
