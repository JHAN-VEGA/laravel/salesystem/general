<?php

namespace Customer\Application\Services\Location;

use Customer\Application\Interfaces\Services\Location\CityServiceInterface;
use Customer\Infrastructure\Interfaces\Repositories\Location\CityRepositoryInterface;
use Illuminate\Support\Collection;

class CityService implements CityServiceInterface
{
    /**
     * @var CityRepositoryInterface
     */
    private CityRepositoryInterface $cityRepo;

    /**
     * @param CityRepositoryInterface $cityRepo
     */
    public function __construct(
        CityRepositoryInterface $cityRepo
    )
    {
        $this->cityRepo = $cityRepo;
    }

    /**
     * @param int $stateId
     * @param string $value
     * @return Collection
     */
    public function searchByStateId(int $stateId, string $value): Collection
    {
        return $this->cityRepo
            ->setUser(auth()->user())
            ->searchByStateId($stateId, $value);
    }

    /**
     * @param string $value
     * @return Collection
     */
    public function search(string $value): Collection
    {
        return $this->cityRepo
            ->setUser(auth()->user())
            ->search($value);
    }


}
