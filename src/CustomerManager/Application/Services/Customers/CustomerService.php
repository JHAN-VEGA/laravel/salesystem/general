<?php

namespace Customer\Application\Services\Customers;

use Customer\Application\Interfaces\Services\Customers\CustomerServiceInterface;
use Customer\Domain\Dto\Customer\CustomerNewDto;
use Customer\Domain\Dto\Customer\CustomerUpdateDto;
use Customer\Domain\Exceptions\CustomerNotFoundException;
use Customer\Infrastructure\Interfaces\Repositories\Customers\CustomerRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class CustomerService implements CustomerServiceInterface
{
    /**
     * @var int
     */
    private int $customerId;

    /**
     * @var object|null
     */
    private ?object $customer;

    /**
     * @var CustomerRepositoryInterface
     */
    private CustomerRepositoryInterface $customerRepo;

    /**
     * @param CustomerRepositoryInterface $customerRepo
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepo
    )
    {
        $this->customerRepo = $customerRepo;
    }

    /**
     * @return int
     */
    public function getCustomerId(): int
    {
        return $this->customerId;
    }

    public function paginate(Request $request)
    {
        $recordsByPage = 20;
        $orderBy = 'id';
        $order = 'desc';

        switch ($request->get('search')) {
            case 'name':
                $this->customerRepo->filterByName($request->get('value'));
                break;
            case 'email':
                $this->customerRepo->filterByEmail($request->get('value'));
                break;
            case 'document_number':
                $this->customerRepo->filterByDocumentNumber($request->get('value'));
                break;
            case 'phone':
                $this->customerRepo->filterByPhone($request->get('value'));
                break;
            case 'address':
                $this->customerRepo->filterByAddress($request->get('value'));
                break;
            case 'state_id':
                $this->customerRepo->filterByStateId($request->get('value'));
                break;
            case 'city_id':
                $this->customerRepo->filterByCityId($request->get('value'));
                break;
            case 'document_type_id':
                $this->customerRepo->filterByDocumentTypeId($request->get('value'));
                break;
            case 'person_type_id':
                $this->customerRepo->filterByPersonTypeId($request->get('value'));
                break;
        }

        return $this->customerRepo
            ->setUser(auth()->user())
            ->paginate($recordsByPage, $orderBy, $order);
    }

    /**
     * @param string $value
     * @return Collection
     */
    public function search(string $value): Collection
    {
        return $this->customerRepo
            ->setUser(auth()->user())
            ->search($value);
    }

    /**
     * @param int $id
     * @return object|null
     */
    public function findById(int $id): object|null
    {
        return $this->customerRepo
            ->setUser(auth()->user())
            ->findById($id);
    }

    /**
     * @param CustomerNewDto $dto
     * @return $this
     */
    public function store(CustomerNewDto $dto): self
    {
        $this->customerId = $this->customerRepo
            ->setUser(auth()->user())
            ->store($dto)
            ->getCustomerId();

        return $this;
    }

    /**
     * @param CustomerUpdateDto $dto
     * @param int $id
     * @return $this
     * @throws \Throwable
     */
    public function update(CustomerUpdateDto $dto): self
    {
        $this->setCustomerById($dto->id);

        $this->customerRepo
            ->setUser(auth()->user())
            ->update($dto);

        return $this;
    }

    /**
     * @param int $id
     * @return $this
     * @throws \Throwable
     */
    protected function setCustomerById(int $id):self
    {
        $this->customer = $this->findById($id);

        throw_if(is_null($this->customer), new CustomerNotFoundException());

        return $this;
    }
}
