<?php

namespace Customer\Application\Services\Documents;

use Customer\Application\Interfaces\Services\Documents\PersonTypeServiceInterface;
use Customer\Infrastructure\Interfaces\Repositories\Documents\PersonTypeRepositoryInterface;
use Illuminate\Support\Collection;

class PersonTypeService implements PersonTypeServiceInterface
{
    /**
     * @var PersonTypeRepositoryInterface
     */
    private PersonTypeRepositoryInterface $personTypeRepo;

    /**
     * @param PersonTypeRepositoryInterface $personTypeRepo
     */
    public function __construct(
        PersonTypeRepositoryInterface $personTypeRepo
    )
    {
        $this->personTypeRepo = $personTypeRepo;
    }

    public function getAll(): Collection
    {
        return $this->personTypeRepo
            ->setUser(auth()->user())
            ->getAll();
    }


}
