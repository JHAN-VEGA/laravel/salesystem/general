<?php

namespace Customer\Application\Services\Documents;

use Customer\Application\Interfaces\Services\Documents\DocumentTypeServiceInterface;
use Customer\Infrastructure\Interfaces\Repositories\Documents\DocumentTypeRepositoryInterface;
use Illuminate\Support\Collection;

class DocumentTypeService implements DocumentTypeServiceInterface
{
    /**
     * @var DocumentTypeRepositoryInterface
     */
    private DocumentTypeRepositoryInterface $documentTypeRepo;

    /**
     * @param DocumentTypeRepositoryInterface $documentTypeRepo
     */
    public function __construct(
        DocumentTypeRepositoryInterface $documentTypeRepo
    )
    {
        $this->documentTypeRepo = $documentTypeRepo;
    }

    public function getAll(): Collection
    {
        return $this->documentTypeRepo
            ->setUser(auth()->user())
            ->getAll();
    }


}
