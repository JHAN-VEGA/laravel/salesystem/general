<?php

namespace Customer\Domain\Dto\Customer;

use Customer\Domain\Dto\BaseDto;

class CustomerNewDto extends BaseDto
{
    /**
     * @var string
     */
    public string $name;

    /**
     * @var string
     */
    public string $documentNumber;

    /**
     * @var string
     */
    public string $phone;

    /**
     * @var string
     */
    public string $email;

    /**
     * @var string
     */
    public string $address;

    /**
     * @var int
     */
    public int $stateId;

    /**
     * @var int
     */
    public int $cityId;

    /**
     * @var int
     */
    public int $documentTypeId;

    /**
     * @var int
     */
    public int $personTypeId;
}
