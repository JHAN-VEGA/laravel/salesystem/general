<?php

namespace Customer\Domain\Exceptions;

class CustomerNotFoundException extends \Exception
{
    /**
     * @var int
     */
    protected $code = 404;

    /**
     * @var string
     */
    protected $message = 'Customer not found exception';
}
