<?php

namespace Customer\Test\Application\Services;

use Customer\Application\Interfaces\Services\Location\CityServiceInterface;
use Customer\Infrastructure\Interfaces\Repositories\Location\CityRepositoryInterface;
use Customer\Infrastructure\Mocks\Repositories\CityRepositoryMock;
use Customer\Test\Base;
use Illuminate\Support\Facades\App;

class CityServiceTest extends Base
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->setNewUser();
    }

    /**
     * @test
     */
    public function isSearchByStateIdWorking()
    {
        $this->instance(
            CityRepositoryInterface::class,
            (App::make(CityRepositoryMock::class))->generateSearchByStateIdWorking()
        );

        $this->actingAs($this->user);

        $cities = (App::make(CityServiceInterface::class))
            ->searchByStateId(1, 'customer');

        $this->assertNotNull($cities);
    }

    /**
     * @test
     */
    public function isSearchWorking()
    {
        $this->instance(
            CityRepositoryInterface::class,
            (App::make(CityRepositoryMock::class))->generateSearchWorking()
        );

        $this->actingAs($this->user);

        $cities = (App::make(CityServiceInterface::class))
            ->search('customer');

        $this->assertNotNull($cities);
    }
}
