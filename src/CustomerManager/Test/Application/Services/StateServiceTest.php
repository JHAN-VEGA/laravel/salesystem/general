<?php

namespace Customer\Test\Application\Services;

use Customer\Application\Interfaces\Services\Location\StateServiceInterface;
use Customer\Infrastructure\Interfaces\Repositories\Location\StateRepositoryInterface;
use Customer\Infrastructure\Mocks\Repositories\StateRepositoryMock;
use Customer\Test\Base;
use Illuminate\Support\Facades\App;

class StateServiceTest extends Base
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->setNewUser();
    }

    /**
     * @test
     */
    public function isSearchByStateIdWorking()
    {
        $this->instance(
            StateRepositoryInterface::class,
            (App::make(StateRepositoryMock::class))->generateSearchByStateIdWorking()
        );

        $this->actingAs($this->user);

        $states = (App::make(StateServiceInterface::class))
            ->search('state');

        $this->assertNotNull($states);
    }
}
