<?php

namespace Customer\Test\Application\Services;

use Customer\Application\Interfaces\Services\Documents\PersonTypeServiceInterface;
use Customer\Infrastructure\Interfaces\Repositories\Documents\PersonTypeRepositoryInterface;
use Customer\Infrastructure\Mocks\Repositories\PersonTypeRepositoryMock;
use Customer\Test\Base;
use Illuminate\Support\Facades\App;

class PersonTypeServiceTest extends Base
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->setNewUser();
    }

    /**
     * @test
     */
    public function isGetAll()
    {
        $this->instance(
            PersonTypeRepositoryInterface::class,
            (App::make(PersonTypeRepositoryMock::class))->generateGetAllWorking()
        );

        $this->actingAs($this->user);

        $personTypes = (App::make(PersonTypeServiceInterface::class))
            ->getAll();

        $this->assertNotNull($personTypes);
    }


}
