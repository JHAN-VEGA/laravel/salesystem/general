<?php

namespace Customer\Test\Application\Services;

use Customer\Application\Interfaces\Services\Documents\DocumentTypeServiceInterface;
use Customer\Infrastructure\Interfaces\Repositories\Documents\DocumentTypeRepositoryInterface;
use Customer\Infrastructure\Mocks\Repositories\DocumentTypeRepositoryMock;
use Customer\Test\Base;
use Illuminate\Support\Facades\App;

class DocumentTypeServiceTest extends Base
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->setNewUser();
    }

    /**
     * @test
     */
    public function isGetAll()
    {
        $this->instance(
            DocumentTypeRepositoryInterface::class,
            (App::make(DocumentTypeRepositoryMock::class))->generateGetAllWorking()
        );

        $this->actingAs($this->user);

        $documentTypes = (App::make(DocumentTypeServiceInterface::class))
            ->getAll();

        $this->assertNotNull($documentTypes);
    }


}
