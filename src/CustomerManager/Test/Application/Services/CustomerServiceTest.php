<?php

namespace Customer\Test\Application\Services;

use Customer\Application\Interfaces\Services\Customers\CustomerServiceInterface;
use Customer\Domain\Dto\Customer\CustomerNewDto;
use Customer\Domain\Dto\Customer\CustomerUpdateDto;
use Customer\Infrastructure\Interfaces\Repositories\Customers\CustomerRepositoryInterface;
use Customer\Infrastructure\Mocks\Repositories\CustomerRepositoryMock;
use Customer\Test\Base;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;

class CustomerServiceTest extends Base
{
    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->setNewUser();
    }

    /**
     * @test
     */
    public function isPaginateWorking()
    {
        $this->instance(
            CustomerRepositoryInterface::class,
            (App::make(CustomerRepositoryMock::class))->generatePaginateWorking()
        );

        $this->actingAs($this->user);

        $customers = (App::make(CustomerServiceInterface::class))
            ->paginate(new Request());

        $this->assertNotNull($customers);
    }

    /**
     * @test
     */
    public function isSearchWorking()
    {
        $this->instance(
            CustomerRepositoryInterface::class,
            (App::make(CustomerRepositoryMock::class))->generateSearchWorking()
        );

        $this->actingAs($this->user);

        $customers = (App::make(CustomerServiceInterface::class))
            ->search('customer');

        $this->assertNotNull($customers);
    }

    /**
     * @test
     */
    public function isFindByIdWorking()
    {
        $this->instance(
            CustomerRepositoryInterface::class,
            (App::make(CustomerRepositoryMock::class))->generateFindByIdWorking()
        );

        $this->actingAs($this->user);

        $customer = (App::make(CustomerServiceInterface::class))
            ->findById(1);

        $this->assertIsObject($customer);
    }

    /**
     * @test
     */
    public function isStoreWorking()
    {
        $this->instance(
            CustomerRepositoryInterface::class,
            (App::make(CustomerRepositoryMock::class))->generateStoreWorking()
        );

        $this->actingAs($this->user);

        $dto = (App::make(CustomerNewDto::class));
        $dto->name = Str::random(10);
        $dto->email = fake()->email;
        $dto->documentNumber = random_int(0, 999999999);
        $dto->phone = fake()->phoneNumber;
        $dto->address = fake()->address;
        $dto->stateId = 1;
        $dto->cityId = 1;
        $dto->documentTypeId = 1;
        $dto->personTypeId = 1;

        $customerId = (App::make(CustomerServiceInterface::class))
            ->store($dto)
            ->getCustomerId();

        $this->assertIsInt($customerId);
    }

    /**
     * @test
     */
    public function isUpdateWorking()
    {
        $this->instance(
            CustomerRepositoryInterface::class,
            (App::make(CustomerRepositoryMock::class))->generateUpdateWorking()
        );

        $this->actingAs($this->user);

        $dto = (App::make(CustomerUpdateDto::class));
        $dto->id = 1;
        $dto->name = Str::random(10);
        $dto->email = fake()->email;
        $dto->documentNumber = random_int(0, 999999999);
        $dto->phone = fake()->phoneNumber;
        $dto->address = fake()->address;
        $dto->stateId = 1;
        $dto->cityId = 1;
        $dto->documentTypeId = 1;
        $dto->personTypeId = 1;

        (App::make(CustomerServiceInterface::class))
            ->update($dto);

        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function isUpdateFailing()
    {
        $this->instance(
            CustomerRepositoryInterface::class,
            (App::make(CustomerRepositoryMock::class))->generateUpdateFailing()
        );

        try {
            $this->actingAs($this->user);

            $dto = (App::make(CustomerUpdateDto::class));
            $dto->id = 1;
            $dto->name = Str::random(10);
            $dto->email = fake()->email;
            $dto->documentNumber = random_int(0, 999999999);
            $dto->phone = fake()->phoneNumber;
            $dto->address = fake()->address;
            $dto->stateId = 1;
            $dto->cityId = 1;
            $dto->documentTypeId = 1;
            $dto->personTypeId = 1;

            (App::make(CustomerServiceInterface::class))
                ->update($dto);
        } catch (\Exception $exception) {
            $this->assertTrue($exception->getCode() === 404);
        }
    }
}
