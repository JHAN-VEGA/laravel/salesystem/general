<?php

namespace Customer\Test\Infrastructure\Repositories;

use Customer\Test\Base;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CityRepositoryTest extends Base
{
    use DatabaseTransactions;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->setNewUser();
        $this->setRepositories();
        $this->beginDatabaseTransaction();
    }

    /**
     * @test
     */
    public function isSearchByStateIdWorking()
    {
        $cities = $this->cityRepo
            ->setUser($this->user)
            ->searchByStateId(7, '153');

        $this->assertNotNull($cities);
    }

    /**
     * @test
     */
    public function isSearchWorking()
    {
        $cities = $this->cityRepo
            ->setUser($this->user)
            ->search('tunja');

        $this->assertNotNull($cities);
    }



}
