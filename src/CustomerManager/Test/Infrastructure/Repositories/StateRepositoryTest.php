<?php

namespace Customer\Test\Infrastructure\Repositories;

use Customer\Test\Base;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StateRepositoryTest extends Base
{
    use DatabaseTransactions;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->setNewUser();
        $this->setRepositories();
        $this->beginDatabaseTransaction();
    }

    /**
     * @test
     */
    public function isSearchWorking()
    {
        $states = $this->stateRepo
            ->setUser($this->user)
            ->search('boyaca');

        $this->assertNotNull($states);
    }



}
