<?php

namespace Customer\Test\Infrastructure\Repositories;

use Customer\Test\Base;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PersonTypeRepositoryTest extends Base
{
    use DatabaseTransactions;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->setNewUser();
        $this->setRepositories();
        $this->beginDatabaseTransaction();
    }

    /**
     * @test
     */
    public function isGetAllWorking()
    {
        $documentTypes = $this->personTypeRepo
            ->setUser($this->user)
            ->getAll();

        $this->assertNotNull($documentTypes);
    }
}
