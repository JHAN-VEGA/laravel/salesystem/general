<?php

namespace Customer\Test\Infrastructure\Repositories;

use Customer\Domain\Dto\Customer\CustomerNewDto;
use Customer\Domain\Dto\Customer\CustomerUpdateDto;
use Customer\Test\Base;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use function PHPUnit\Framework\assertNotNull;

class CustomerRepositoryTest extends Base
{
    use DatabaseTransactions;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->setNewUser();
        $this->setRepositories();
        $this->beginDatabaseTransaction();
    }

    /**
     * @test
     */
    public function isPaginateWorking()
    {
        $this->createCustomerEntity();

        $customers = $this->customerRepo
            ->setUser($this->user)
            ->paginate(20, 'id', 'desc');

        self::assertNotNull($customers);
    }

    /**
     * @test
     */
    public function isSearchWorking()
    {
        $this->createCustomerEntity();

        $customers = $this->customerRepo
            ->setUser($this->user)
            ->search('customer');

        self::assertNotNull($customers);
    }

    /**
     * @test
     */
    public function isFindByIdWorking()
    {
        $this->createCustomerEntity();

        $customer = $this->customerRepo
            ->setUser($this->user)
            ->findById($this->customerId);

        assertNotNull($customer);
    }

    /**
     * @test
     */
    public function isStoreWorking()
    {
        $dto = (App::make(CustomerNewDto::class));
        $dto->name = Str::random(10);
        $dto->email = fake()->email;
        $dto->documentNumber = random_int(0, 999999999);
        $dto->phone = fake()->phoneNumber;
        $dto->address = fake()->address;
        $dto->stateId = 1;
        $dto->cityId = 1;
        $dto->documentTypeId = 1;
        $dto->personTypeId = 1;

        $customerId = $this->customerRepo
            ->setUser($this->user)
            ->store($dto)
            ->getCustomerId();

        $this->assertDatabaseHas($this->customerRepo->getTableName(), [
            'id' => $customerId,
            'name' => $dto->name,
            'email' => $dto->email,
            'document_number' => $dto->documentNumber,
            'phone' => $dto->phone,
            'address' => $dto->address,
            'state_id' => $dto->stateId,
            'city_id' => $dto->cityId,
            'document_type_id' => $dto->documentTypeId,
            'person_type_id' => $dto->personTypeId,
        ], $this->customerRepo->getDatabaseConnection());

    }

    /**
     * @test
     */
    public function isUpdateWorking()
    {
        $this->createCustomerEntity();

        $dto = (App::make(CustomerUpdateDto::class));
        $dto->id = $this->customerId;
        $dto->name = Str::random(10);
        $dto->email = fake()->email;
        $dto->documentNumber = random_int(0, 999999999);
        $dto->phone = fake()->phoneNumber;
        $dto->address = fake()->address;
        $dto->stateId = 1;
        $dto->cityId = 1;
        $dto->documentTypeId = 1;
        $dto->personTypeId = 1;

        $this->customerRepo
            ->setUser($this->user)
            ->update($dto);

        $this->assertDatabaseHas($this->customerRepo->getTableName(), [
            'id' => $this->customerId,
            'name' => $dto->name,
            'email' => $dto->email,
            'document_number' => $dto->documentNumber,
            'phone' => $dto->phone,
            'address' => $dto->address,
            'state_id' => $dto->stateId,
            'city_id' => $dto->cityId,
            'document_type_id' => $dto->documentTypeId,
            'person_type_id' => $dto->personTypeId,
        ], $this->customerRepo->getDatabaseConnection());

    }
}
