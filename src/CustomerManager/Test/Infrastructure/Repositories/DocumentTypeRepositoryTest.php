<?php

namespace Customer\Test\Infrastructure\Repositories;

use Customer\Test\Base;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DocumentTypeRepositoryTest extends Base
{
    use DatabaseTransactions;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->setNewUser();
        $this->setRepositories();
        $this->beginDatabaseTransaction();
    }

    /**
     * @test
     */
    public function isGetAllWorking()
    {
        $documentTypes = $this->documentTypeRepo
            ->setUser($this->user)
            ->getAll();

        $this->assertNotNull($documentTypes);
    }
}
