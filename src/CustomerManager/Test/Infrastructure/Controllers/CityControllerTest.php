<?php

namespace Customer\Test\Infrastructure\Controllers;

use Auth\Application\Interfaces\AuthServiceInterface;
use Auth\Application\Mocks\Services\AuthServiceMock;
use Customer\Application\Interfaces\Services\Location\CityServiceInterface;
use Customer\Application\Mocks\Services\CityServiceMock;
use Customer\Test\Base;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;

class CityControllerTest extends Base
{
    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->setNewUser();

        $this->instance(
            AuthServiceInterface::class,
            (App::make(AuthServiceMock::class))->generateLoginUserFromTokenWorking()
        );

        $this->withHeaders([
            'Authorization' => 'Bearer ' . Str::random(30)
        ]);
    }

    /**
     * @test
     */
    public function isSearchWorking()
    {
        $this->instance(
            CityServiceInterface::class,
            (App::make(CityServiceMock::class))->generateSearchWorking()
        );

        $this->actingAs($this->user);

        $response = $this->get(route('cities.search', [
            'value' => 'city'
        ]));

        $response->assertJsonStructure(['success', 'code', 'message', 'cities']);
    }

    /**
     * @test
     */
    public function isSearchByStateWorking()
    {
        $this->instance(
            CityServiceInterface::class,
            (App::make(CityServiceMock::class))->generateSearchByStateIdWorking()
        );

        $this->actingAs($this->user);

        $response = $this->get(route('cities.searchByStateId', [
            'state_id' => 1,
            'value' => 'city',
        ]));

        $response->assertJsonStructure(['success', 'code', 'message', 'cities']);
    }
}
