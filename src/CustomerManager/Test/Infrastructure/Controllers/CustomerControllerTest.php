<?php

namespace Customer\Test\Infrastructure\Controllers;

use Auth\Application\Interfaces\AuthServiceInterface;
use Auth\Application\Mocks\Services\AuthServiceMock;
use Customer\Application\Interfaces\Services\Customers\CustomerServiceInterface;
use Customer\Application\Mocks\Services\CustomerServiceMock;
use Customer\Test\Base;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;

class CustomerControllerTest extends Base
{
    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->setNewUser();

        $this->instance(
            AuthServiceInterface::class,
            (App::make(AuthServiceMock::class))->generateLoginUserFromTokenWorking()
        );

        $this->withHeaders([
            'Authorization' => 'Bearer ' . Str::random(30)
        ]);
    }

    /**
     * @test
     */
    public function isPaginateWorking()
    {
        $this->instance(
            CustomerServiceInterface::class,
            (App::make(CustomerServiceMock::class))->generatePaginateWorking()
        );

        $this->actingAs($this->user);

        $response = $this->get(route('customers.paginate'));

        $response->assertJsonStructure(['success', 'code', 'message', 'customers']);
    }

    /**
     * @test
     */
    public function isSearchWorking()
    {
        $this->instance(
            CustomerServiceInterface::class,
            (App::make(CustomerServiceMock::class))->generateSearchWorking()
        );

        $this->actingAs($this->user);

        $response = $this->get(route('customers.search', [
            'value' => 'customer'
        ]));

        $response->assertJsonStructure(['success', 'code', 'message', 'customers']);
    }

    /**
     * @test
     */
    public function isFindByIdWorking()
    {
        $this->instance(
            CustomerServiceInterface::class,
            (App::make(CustomerServiceMock::class))->generateFindByIdWorking()
        );

        $this->actingAs($this->user);

        $response = $this->get(route('customers.show', [1]));

        $response->assertJsonStructure(['success', 'code', 'message', 'customer']);
    }

    /**
     * @test
     */
    public function isStoreWorking()
    {
        $this->instance(
            CustomerServiceInterface::class,
            (App::make(CustomerServiceMock::class))->generateStoreWorking()
        );

        $this->actingAs($this->user);

        $response = $this->post(route('customers.store'), [
            'name' => 'Customer name',
            'document_number' => '123456789',
            'phone' => '3216549870',
            'email' => 'customer@cuatomer',
            'address' => 'Address',
            'state_id' => 1,
            'city_id' => 1,
            'document_type_id' => 1,
            'person_type_id' => 1,
        ]);

        $response->assertJsonStructure(['success', 'code', 'message', 'customer_id']);
    }

    /**
     * @test
     */
    public function isUpdateWorking()
    {
        $this->instance(
            CustomerServiceInterface::class,
            (App::make(CustomerServiceMock::class))->generateUpdateWorking()
        );

        $this->actingAs($this->user);

        $response = $this->post(route('customers.update', [1]), [
            'name' => 'Customer name',
            'document_number' => '123456789',
            'phone' => '3216549870',
            'email' => 'customer@cuatomer',
            'address' => 'Address',
            'state_id' => 1,
            'city_id' => 1,
            'document_type_id' => 1,
            'person_type_id' => 1,
        ]);

        $response->assertJsonStructure(['success', 'code', 'message']);
    }

}
