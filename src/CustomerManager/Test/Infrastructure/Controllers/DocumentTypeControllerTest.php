<?php

namespace Customer\Test\Infrastructure\Controllers;

use Auth\Application\Interfaces\AuthServiceInterface;
use Auth\Application\Mocks\Services\AuthServiceMock;
use Customer\Application\Interfaces\Services\Documents\DocumentTypeServiceInterface;
use Customer\Application\Mocks\Services\DocumentTypeServiceMock;
use Customer\Test\Base;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;

class DocumentTypeControllerTest extends Base
{
    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->setNewUser();

        $this->instance(
            AuthServiceInterface::class,
            (App::make(AuthServiceMock::class))->generateLoginUserFromTokenWorking()
        );

        $this->withHeaders([
            'Authorization' => 'Bearer ' . Str::random(30)
        ]);
    }

    /**
     * @test
     */
    public function isGetAllWorking()
    {
        $this->instance(
            DocumentTypeServiceInterface::class,
            (App::make(DocumentTypeServiceMock::class))->generateGetAllWorking()
        );

        $this->actingAs($this->user);

        $response = $this->get(route('documentTypes.getAll'));

        $response->assertJsonStructure(['success', 'code', 'message', 'document_types']);

    }
}
