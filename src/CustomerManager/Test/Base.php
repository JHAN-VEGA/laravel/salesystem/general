<?php

namespace Customer\Test;

use App\Models\User;
use Customer\Infrastructure\Interfaces\Repositories\Customers\CustomerRepositoryInterface;
use Customer\Infrastructure\Interfaces\Repositories\Documents\DocumentTypeRepositoryInterface;
use Customer\Infrastructure\Interfaces\Repositories\Documents\PersonTypeRepositoryInterface;
use Customer\Infrastructure\Interfaces\Repositories\Location\CityRepositoryInterface;
use Customer\Infrastructure\Interfaces\Repositories\Location\StateRepositoryInterface;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Tests\TestCase;

abstract class Base extends TestCase
{
    /**
     * @var int
     */
    protected int $customerId;

    /**
     * @var CustomerRepositoryInterface
     */
    protected CustomerRepositoryInterface $customerRepo;

    /**
     * @var DocumentTypeRepositoryInterface
     */
    protected DocumentTypeRepositoryInterface $documentTypeRepo;

    /**
     * @var PersonTypeRepositoryInterface
     */
    protected PersonTypeRepositoryInterface $personTypeRepo;

    /**
     * @var CityRepositoryInterface
     */
    protected CityRepositoryInterface $cityRepo;

    /**
     * @var StateRepositoryInterface
     */
    protected StateRepositoryInterface $stateRepo;

    /**
     * @var User
     */
    protected User $user;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
    }

    protected function setNewUser():void
    {
        $this->user = new User();
        $this->user->id = 1;
    }

    public function createCustomerEntity():void
    {
        $this->customerId = DB::connection($this->customerRepo->getDatabaseConnection())
            ->table($this->customerRepo->getTableName())
            ->insertGetId([
                'name' => 'Customer name',
                'email' => fake()->email,
                'phone' => fake()->phoneNumber,
                'address' => fake()->address,
                'document_number' => random_int(0, 999999999),
                'state_id' => 1,
                'city_id' => 1,
                'document_type_id' => 1,
                'person_type_id' => 1,
            ]);
    }

    /**
     * @return void
     */
    protected function setRepositories():void
    {
        $this->customerRepo = (App::make(CustomerRepositoryInterface::class));
        $this->documentTypeRepo = (App::make(DocumentTypeRepositoryInterface::class));
        $this->personTypeRepo = (App::make(PersonTypeRepositoryInterface::class));
        $this->cityRepo = (App::make(CityRepositoryInterface::class));
        $this->stateRepo = (App::make(StateRepositoryInterface::class));
    }
}
