<?php

namespace Customer\Infrastructure\Interfaces\Repositories\Customers;

use Customer\Domain\Dto\Customer\CustomerNewDto;
use Customer\Domain\Dto\Customer\CustomerUpdateDto;
use Customer\Infrastructure\Interfaces\Repositories\BaseRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface CustomerRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * @return int
     */
    public function getCustomerId():int;

    /**
     * @param int $recordsByPage
     * @param string $orderBy
     * @param string $order
     * @return LengthAwarePaginator
     */
    public function paginate(
        int $recordsByPage = 20, string $orderBy = 'created_at', string $order = 'desc'
    ):LengthAwarePaginator;

    /**
     * @param string $value
     * @return Collection
     */
    public function search(string $value): Collection;

    /**
     * @param int $id
     * @return object|null
     */
    public function findById(int $id): object|null;

    /**
     * @param string $value
     * @return self
     */
    public function filterByName(string $value):self;

    /**
     * @param string $value
     * @return self
     */
    public function filterByEmail(string $value):self;

    /**
     * @param string $value
     * @return self
     */
    public function filterByDocumentNumber(string $value):self;

    /**
     * @param string $value
     * @return self
     */
    public function filterByPhone(string $value):self;

    /**
     * @param string $value
     * @return self
     */
    public function filterByAddress(string $value):self;

    /**
     * @param int $stateId
     * @return self
     */
    public function filterByStateId(int $stateId):self;

    /**
     * @param int $cityId
     * @return self
     */
    public function filterByCityId(int $cityId):self;

    /**
     * @param int $documentTypeId
     * @return self
     */
    public function filterByDocumentTypeId(int $documentTypeId):self;

    /**
     * @param int $personTypeId
     * @return self
     */
    public function filterByPersonTypeId(int $personTypeId):self;

    /**
     * @param CustomerNewDto $dto
     * @return self
     */
    public function store(CustomerNewDto $dto): self;

    /**
     * @param CustomerUpdateDto $dto
     * @return self
     */
    public function update(CustomerUpdateDto $dto): self;
}
