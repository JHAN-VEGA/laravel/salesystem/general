<?php

namespace Customer\Infrastructure\Interfaces\Repositories\Location;

use Customer\Infrastructure\Interfaces\Repositories\BaseRepositoryInterface;
use Illuminate\Support\Collection;

interface StateRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * @param string $value
     * @return Collection
     */
    public function search(string $value):Collection;
}
