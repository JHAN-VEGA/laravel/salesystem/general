<?php

namespace Customer\Infrastructure\Interfaces\Repositories\Location;

use Customer\Infrastructure\Interfaces\Repositories\BaseRepositoryInterface;
use Illuminate\Support\Collection;

interface CityRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * @param int $stateId
     * @param string $value
     * @return Collection
     */
    public function searchByStateId(int $stateId, string $value):Collection;

    /**
     * @param string $value
     * @return Collection
     */
    public function search(string $value):Collection;
}
