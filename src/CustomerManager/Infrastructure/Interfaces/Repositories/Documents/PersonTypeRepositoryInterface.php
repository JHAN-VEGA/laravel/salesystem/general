<?php

namespace Customer\Infrastructure\Interfaces\Repositories\Documents;

use Customer\Infrastructure\Interfaces\Repositories\BaseRepositoryInterface;
use Illuminate\Support\Collection;

interface PersonTypeRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * @return Collection
     */
    public function getAll():Collection;
}
