<?php

namespace Customer\Infrastructure\Interfaces\Repositories\Documents;

use Customer\Infrastructure\Interfaces\Repositories\BaseRepositoryInterface;
use Illuminate\Support\Collection;

interface DocumentTypeRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * @return Collection
     */
    public function getAll():Collection;
}
