<?php

namespace Customer\Infrastructure\Controllers\Location;

use Customer\Application\Interfaces\Services\Location\StateServiceInterface;
use Customer\Infrastructure\Controllers\BaseController;
use Illuminate\Http\Request;

class StateController extends BaseController
{
    /**
     * @var StateServiceInterface
     */
    private StateServiceInterface $stateService;

    /**
     * @param StateServiceInterface $stateService
     */
    public function __construct(
        StateServiceInterface $stateService
    )
    {
        $this->stateService = $stateService;
    }

    public function search(Request $request)
    {
        $request->validate([
            'value' => ['required', 'string']
        ]);

        return $this->executeWithJsonSuccessResponse(function () use ($request) {
            return [
                'message' => 'States list',
                'states' => $this->stateService->search($request->get('value'))
            ];
        });
    }
}
