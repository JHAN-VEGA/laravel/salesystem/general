<?php

namespace Customer\Infrastructure\Controllers\Location;

use Customer\Application\Interfaces\Services\Location\CityServiceInterface;
use Customer\Infrastructure\Controllers\BaseController;
use Illuminate\Http\Request;

class CityController extends BaseController
{
    /**
     * @var CityServiceInterface
     */
    private  CityServiceInterface $cityService;

    /**
     * @param CityServiceInterface $cityService
     */
    public function __construct(
        CityServiceInterface $cityService
    )
    {
        $this->cityService = $cityService;
    }

    public function searchByStateId(Request $request)
    {
        $request->validate([
            'state_id' =>  ['required', 'integer'],
            'value' =>  ['required', 'string'],
        ]);

        return $this->executeWithJsonSuccessResponse(function () use ($request) {
            return [
                'message' => 'Cities list',
                'cities' => $this->cityService
                    ->searchByStateId($request->get('state_id'), $request->get('value'))
            ];
        });
    }

    public function search(Request $request)
    {
        $request->validate([
            'value' =>  ['required', 'string'],
        ]);

        return $this->executeWithJsonSuccessResponse(function () use ($request) {
            return [
                'message' => 'Cities list',
                'cities' => $this->cityService
                    ->search($request->get('value'))
            ];
        });
    }

}
