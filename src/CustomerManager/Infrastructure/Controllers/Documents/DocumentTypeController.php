<?php

namespace Customer\Infrastructure\Controllers\Documents;

use Customer\Application\Interfaces\Services\Documents\DocumentTypeServiceInterface;
use Customer\Infrastructure\Controllers\BaseController;

class DocumentTypeController extends BaseController
{
    /**
     * @var DocumentTypeServiceInterface
     */
    private DocumentTypeServiceInterface $documentTypeService;

    /**
     * @param DocumentTypeServiceInterface $documentTypeService
     */
    public function __construct(
        DocumentTypeServiceInterface $documentTypeService
    )
    {
        $this->documentTypeService = $documentTypeService;
    }

    public function getAll()
    {
        return $this->executeWithJsonSuccessResponse(function () {
            return [
                'document_types' => $this->documentTypeService->getAll()
            ];
        });
    }
}
