<?php

namespace Customer\Infrastructure\Controllers\Documents;

use Customer\Application\Interfaces\Services\Documents\PersonTypeServiceInterface;
use Customer\Infrastructure\Controllers\BaseController;

class PersonTypeController extends BaseController
{
    /**
     * @var PersonTypeServiceInterface
     */
    private PersonTypeServiceInterface $personTypeService;

    /**
     * @param PersonTypeServiceInterface $personTypeService
     */
    public function __construct(
        PersonTypeServiceInterface $personTypeService
    )
    {
        $this->personTypeService = $personTypeService;
    }

    public function getAll()
    {
        return $this->executeWithJsonSuccessResponse(function () {
            return [
                'person_types' => $this->personTypeService->getAll()
            ];
        });
    }
}
