<?php

namespace Customer\Infrastructure\Controllers\Customers;

use Customer\Application\Interfaces\Services\Customers\CustomerServiceInterface;
use Customer\Application\Mappers\Customer\CustomerNewDtoMapper;
use Customer\Application\Mappers\Customer\CustomerUpdateDtoMapper;
use Customer\Infrastructure\Controllers\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
class CustomerController extends BaseController
{
    /**
     * @var CustomerServiceInterface
     */
    private CustomerServiceInterface $customerService;

    /**
     * @param CustomerServiceInterface $customerService
     */
    public function __construct(
        CustomerServiceInterface $customerService
    )
    {
        $this->customerService = $customerService;
    }

    public function paginate(Request $request)
    {
        return $this->executeWithJsonSuccessResponse(function () use ($request) {
            return [
                'message' => 'Customer',
                'customers' => $this->customerService
                    ->paginate($request)
            ];
        });
    }

    public function search(Request $request)
    {
        return $this->executeWithJsonSuccessResponse(function () use ($request) {
            return [
                'message' => 'Customer',
                'customers' => $this->customerService
                    ->search($request->get('value'))
            ];
        });
    }

    public function show(int $id)
    {
        return $this->executeWithJsonSuccessResponse(function () use ($id) {
            return [
                'message' => 'Customer',
                'customer' => $this->customerService
                    ->findById($id)
            ];
        });
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'document_number' => ['required', 'string'],
            'phone' => ['required', 'string'],
            'email' => ['required', 'email'],
            'address' => ['required', 'string'],
            'state_id' => ['required', 'integer'],
            'city_id' => ['required', 'integer'],
            'document_type_id' => ['required', 'integer'],
            'person_type_id' => ['required', 'integer'],
        ]);

        return $this->executeWithJsonSuccessResponse(function () use ($request) {

            $dto = (App::make(CustomerNewDtoMapper::class))
                ->createFromRequest($request);

            return [
                'message' => 'Created customer',
                'customer_id' => $this->customerService
                    ->store($dto)
                    ->getCustomerId()
            ];
        });
    }

    public function update(Request $request, int $id)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'document_number' => ['required', 'string'],
            'phone' => ['required', 'string'],
            'email' => ['required', 'email'],
            'address' => ['required', 'string'],
            'state_id' => ['required', 'integer'],
            'city_id' => ['required', 'integer'],
            'document_type_id' => ['required', 'integer'],
            'person_type_id' => ['required', 'integer'],
        ]);

        return $this->executeWithJsonSuccessResponse(function () use ($request, $id) {

            $dto = (App::make(CustomerUpdateDtoMapper::class))
                ->updateFromRequest($request, $id);

            $this->customerService
                ->update($dto);

            return [
                'message' => 'Updated customer',
            ];
        });
    }

}
