<?php

namespace Customer\Infrastructure\Mocks\Repositories;

use Customer\Infrastructure\Interfaces\Repositories\Documents\DocumentTypeRepositoryInterface;

class DocumentTypeRepositoryMock
{
    public function generateGetAllWorking()
    {
        $mock = \Mockery::mock(DocumentTypeRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('getAll')
            ->once()
            ->andReturnUsing(function () {
                $documentType = new \stdClass();
                $documentType->id = 1;
                $documentType->name = 'Document type name';

                return collect([$documentType]);
            });

        return $mock;
    }

}
