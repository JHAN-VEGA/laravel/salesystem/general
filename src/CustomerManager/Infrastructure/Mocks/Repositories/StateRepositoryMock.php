<?php

namespace Customer\Infrastructure\Mocks\Repositories;

use Customer\Infrastructure\Interfaces\Repositories\Location\StateRepositoryInterface;

class StateRepositoryMock
{
    public function generateSearchByStateIdWorking()
    {
        $mock = \Mockery::mock(StateRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('search')
            ->once()
            ->andReturnUsing(function () {
                $state = new \stdClass();
                $state->id = 1;
                $state->name = 'State name';
                return collect([$state]);
            });

        return $mock;
    }
}
