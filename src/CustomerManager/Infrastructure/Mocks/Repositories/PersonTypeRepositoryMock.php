<?php

namespace Customer\Infrastructure\Mocks\Repositories;

use Customer\Infrastructure\Interfaces\Repositories\Documents\PersonTypeRepositoryInterface;

class PersonTypeRepositoryMock
{
    public function generateGetAllWorking()
    {
        $mock = \Mockery::mock(PersonTypeRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('getAll')
            ->once()
            ->andReturnUsing(function () {
                $personType = new \stdClass();
                $personType->id = 1;
                $personType->name = 'Person type name';

                return collect([$personType]);
            });

        return $mock;
    }

}
