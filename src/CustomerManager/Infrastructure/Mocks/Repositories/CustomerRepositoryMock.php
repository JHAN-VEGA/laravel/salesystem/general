<?php

namespace Customer\Infrastructure\Mocks\Repositories;

use Customer\Infrastructure\Interfaces\Repositories\Customers\CustomerRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator;

class CustomerRepositoryMock
{
    public function generatePaginateWorking()
    {
        $mock = \Mockery::mock(CustomerRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('paginate')
            ->once()
            ->andReturnUsing(function () {
                $customer = new \stdClass();
                $customer->id = 1;
                $customer->name = 'Customer name';

                return new LengthAwarePaginator([$customer], 1, 1, 1);
            });

        return $mock;
    }

    public function generateSearchWorking()
    {
        $mock = \Mockery::mock(CustomerRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('search')
            ->once()
            ->andReturnUsing(function () {
                $customer = new \stdClass();
                $customer->id = 1;
                $customer->name = 'Customer name';

                return collect([$customer]);
            });

        return $mock;
    }

    public function generateFindByIdWorking()
    {
        $mock = \Mockery::mock(CustomerRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('findById')
            ->once()
            ->andReturnUsing(function () {
                $customer = new \stdClass();
                $customer->id = 1;
                $customer->name = 'Customer name';
                return $customer;
            });

        return $mock;
    }

    public function generateStoreWorking()
    {
        $mock = \Mockery::mock(CustomerRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('store')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('getCustomerId')
            ->once()
            ->andReturn(1);

        return $mock;
    }

    public function generateUpdateWorking()
    {
        $mock = \Mockery::mock(CustomerRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->times(2)
            ->andReturnSelf();

        $mock->shouldReceive('findById')
            ->once()
            ->andReturnUsing(function () {
                $customer = new \stdClass();
                $customer->id = 1;
                $customer->name = 'Customer name';
                return $customer;
            });

        $mock->shouldReceive('update')
            ->once()
            ->andReturnSelf();

        return $mock;
    }

    public function generateUpdateFailing()
    {
        $mock = \Mockery::mock(CustomerRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('findById')
            ->once()
            ->andReturnNull();

        return $mock;
    }
}
