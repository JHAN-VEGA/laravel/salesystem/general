<?php

namespace Customer\Infrastructure\Mocks\Repositories;

use Customer\Infrastructure\Interfaces\Repositories\Location\CityRepositoryInterface;

class CityRepositoryMock
{
    public function generateSearchByStateIdWorking()
    {
        $mock = \Mockery::mock(CityRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('searchByStateId')
            ->once()
            ->andReturnUsing(function () {
                $city = new \stdClass();
                $city->id = 1;
                $city->name = 'City name';
                return collect([$city]);
            });

        return $mock;
    }

    public function generateSearchWorking()
    {
        $mock = \Mockery::mock(CityRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('search')
            ->once()
            ->andReturnUsing(function () {
                $city = new \stdClass();
                $city->id = 1;
                $city->name = 'City name';
                return collect([$city]);
            });

        return $mock;
    }
}
