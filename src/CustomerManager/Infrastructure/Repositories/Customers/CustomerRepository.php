<?php

namespace Customer\Infrastructure\Repositories\Customers;

use Customer\Domain\Dto\Customer\CustomerNewDto;
use Customer\Domain\Dto\Customer\CustomerUpdateDto;
use Customer\Infrastructure\Interfaces\Repositories\Customers\CustomerRepositoryInterface;
use Customer\Infrastructure\Repositories\BaseRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class CustomerRepository extends BaseRepository implements CustomerRepositoryInterface
{
    /**
     * @var string
     */
    protected string $databaseConnection = 'pgsql';

    /**
     * @var string
     */
    protected string $tableName = 'general.customers';

    /**
     * @var int
     */
    private int $customerId;

    /**
     * @return int
     */
    public function getCustomerId(): int
    {
        return $this->customerId;
    }

    /**
     * @return void
     */
    private function baseSelectQuery(): void
    {
        $this->query
            ->select([
                'customers.id AS id',
                'customers.name AS name',
                'customers.email AS email',
                'customers.phone AS phone',
                'customers.address AS address',
                'customers.document_number AS document_number',
                'customers.state_id AS state_id',
                'states.name AS state_name',
                'customers.city_id AS city_id',
                'cities.name AS city_name',
                'customers.document_type_id AS document_type_id',
                'document_types.name AS document_type_name',
                'document_types.code AS document_type_code',
                'customers.person_type_id AS person_type_id',
                'person_types.name AS person_type_name',
            ])
            ->leftJoin('general.states', 'states.id', '=','customers.state_id')
            ->leftJoin('general.cities', 'cities.id', '=','customers.city_id')
            ->leftJoin('general.document_types', 'document_types.id', '=','customers.document_type_id')
            ->leftJoin('general.person_types', 'person_types.id', '=','customers.person_type_id');
    }

    /**
     * @param int $recordsByPage
     * @param string $orderBy
     * @param string $order
     * @return LengthAwarePaginator
     */
    public function paginate(
        int $recordsByPage = 20, string $orderBy = 'created_at', string $order = 'desc'
    ): LengthAwarePaginator
    {
        $this->baseSelectQuery();

        return $this->query
            ->orderBy($this->tableName.'.'. $orderBy, $order)
            ->paginate($recordsByPage)
            ->appends(request()->query());
    }

    /**
     * @param string $value
     * @return Collection
     */
    public function search(string $value): Collection
    {
        $this->baseSelectQuery();

        return $this->query
            ->where('customers.name', 'ilike', '%'.$value.'%')
            ->orWhere('customers.email', 'ilike', '%'.$value.'%')
            ->orWhere('customers.phone', 'ilike', '%'.$value.'%')
            ->orWhere('customers.address', 'ilike', '%'.$value.'%')
            ->orWhere('customers.document_number', 'ilike', $value)
            ->orderBy($this->tableName.'.id', 'desc')
            ->limit(20)
            ->get();
    }

    /**
     * @param int $id
     * @return object|null
     */
    public function findById(int $id): object|null
    {
        $this->baseSelectQuery();

        return $this->query
            ->where('customers.id', '=', $id)
            ->first();
    }

    /**
     * @param string $value
     * @return CustomerRepositoryInterface
     */
    public function filterByName(string $value): CustomerRepositoryInterface
    {
        $this->query
            ->where('customers.name', 'ilike', $value);

        return $this;
    }

    /**
     * @param string $value
     * @return CustomerRepositoryInterface
     */
    public function filterByEmail(string $value): CustomerRepositoryInterface
    {
        $this->query
            ->where('customers.email', 'ilike', $value);

        return $this;
    }

    /**
     * @param string $value
     * @return CustomerRepositoryInterface
     */
    public function filterByDocumentNumber(string $value): CustomerRepositoryInterface
    {
        $this->query
            ->where('customers.document_number', 'ilike', $value);

        return $this;
    }

    /**
     * @param string $value
     * @return CustomerRepositoryInterface
     */
    public function filterByPhone(string $value): CustomerRepositoryInterface
    {
        $this->query
            ->where('customers.phone', 'ilike', $value);

        return $this;
    }

    /**
     * @param string $value
     * @return CustomerRepositoryInterface
     */
    public function filterByAddress(string $value): CustomerRepositoryInterface
    {
        $this->query
            ->where('customers.address', 'ilike', $value);

        return $this;
    }

    /**
     * @param int $stateId
     * @return CustomerRepositoryInterface
     */
    public function filterByStateId(int $stateId): CustomerRepositoryInterface
    {
        $this->query
            ->where('customers.state_id', '=', $stateId);

        return $this;
    }

    /**
     * @param int $cityId
     * @return CustomerRepositoryInterface
     */
    public function filterByCityId(int $cityId): CustomerRepositoryInterface
    {
        $this->query
            ->where('customers.city_id', '=', $cityId);

        return $this;
    }

    /**
     * @param int $documentTypeId
     * @return CustomerRepositoryInterface
     */
    public function filterByDocumentTypeId(int $documentTypeId): CustomerRepositoryInterface
    {
        $this->query
            ->where('customers.document_type_id', '=', $documentTypeId);

        return $this;
    }

    /**
     * @param int $personTypeId
     * @return CustomerRepositoryInterface
     */
    public function filterByPersonTypeId(int $personTypeId): CustomerRepositoryInterface
    {
        $this->query
            ->where('customers.person_type_id', '=', $personTypeId);

        return $this;
    }

    /**
     * @param CustomerNewDto $dto
     * @return $this
     */
    public function store(CustomerNewDto $dto): self
    {
        $this->customerId = $this->query
            ->insertGetId([
                'name' => $dto->name,
                'email' => $dto->email,
                'phone' => $dto->phone,
                'address' => $dto->address,
                'document_number' => $dto->documentNumber,
                'state_id' => $dto->stateId,
                'city_id' => $dto->cityId,
                'document_type_id' => $dto->documentTypeId,
                'person_type_id' => $dto->personTypeId,
                'user_who_created_id' => $this->user->id,
                'created_at' => 'now()'
            ]);

        return $this;
    }

    /**
     * @param CustomerUpdateDto $dto
     * @return $this
     */
    public function update(CustomerUpdateDto $dto): self
    {
        $this->query
            ->where('id', '=', $dto->id)
            ->update([
                'name' => $dto->name,
                'email' => $dto->email,
                'phone' => $dto->phone,
                'address' => $dto->address,
                'document_number' => $dto->documentNumber,
                'state_id' => $dto->stateId,
                'city_id' => $dto->cityId,
                'document_type_id' => $dto->documentTypeId,
                'person_type_id' => $dto->personTypeId,
            ]);

        return $this;
    }

}
