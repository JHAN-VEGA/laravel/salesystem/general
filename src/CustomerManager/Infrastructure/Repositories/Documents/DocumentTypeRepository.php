<?php

namespace Customer\Infrastructure\Repositories\Documents;

use Customer\Infrastructure\Interfaces\Repositories\Documents\DocumentTypeRepositoryInterface;
use Customer\Infrastructure\Repositories\BaseRepository;
use Illuminate\Support\Collection;

class DocumentTypeRepository extends BaseRepository implements DocumentTypeRepositoryInterface
{
    /**
     * @var string
     */
    protected string $databaseConnection = 'pgsql';

    /**
     * @var string
     */
    protected string $tableName = 'general.document_types';

    /**
     * @return Collection
     */
    public function getAll():Collection
    {
        return $this->query
            ->select([
                'document_types.id AS id',
                'document_types.name AS name',
                'document_types.code AS code',
            ])
            ->get();
    }
}
