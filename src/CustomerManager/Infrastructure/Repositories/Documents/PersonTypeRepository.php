<?php

namespace Customer\Infrastructure\Repositories\Documents;

use Customer\Infrastructure\Interfaces\Repositories\Documents\PersonTypeRepositoryInterface;
use Customer\Infrastructure\Repositories\BaseRepository;
use Illuminate\Support\Collection;

class PersonTypeRepository extends BaseRepository implements PersonTypeRepositoryInterface
{
    protected string $databaseConnection = 'pgsql';

    protected string $tableName = 'general.person_types';

    /**
     * @return Collection
     */
    public function getAll():Collection
    {
        return $this->query
            ->select([
                'person_types.id AS id',
                'person_types.name AS name'
            ])
            ->get();
    }
}
