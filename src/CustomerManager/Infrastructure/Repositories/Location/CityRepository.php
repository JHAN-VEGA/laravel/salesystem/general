<?php

namespace Customer\Infrastructure\Repositories\Location;

use Customer\Infrastructure\Interfaces\Repositories\Location\CityRepositoryInterface;
use Customer\Infrastructure\Repositories\BaseRepository;
use Illuminate\Support\Collection;

class CityRepository extends BaseRepository implements CityRepositoryInterface
{
    /**
     * @var string
     */
    protected string $databaseConnection = 'pgsql';

    /**
     * @var string
     */
    protected string $tableName = 'general.cities';

    /**
     * @return void
     */
    private function baseSelectQuery():void
    {
        $this->query
            ->select([
                'cities.id AS id',
                'cities.name AS name',
                'cities.code AS code',
                'cities.dane_code AS dane_code',
                'states.id AS state_id',
                'states.name AS state_name',
                'states.code AS state_code',
                'states.dane_code AS state_dane_code',
                'countries.id AS country_id',
                'countries.name AS country_name',
                'countries.iso_code AS country_iso_code',
            ])
            ->leftJoin('general.states', 'states.id', '=', 'cities.state_id')
            ->leftJoin('general.countries', 'countries.id', '=', 'states.country_id');
    }

    /**
     * @param int $stateId
     * @param string $value
     * @return Collection
     */
    public function searchByStateId(int $stateId, string $value):Collection
    {
        $this->baseSelectQuery();

        return $this->query
            ->where('cities.state_id', '=', $stateId)
            ->where(function ($query) use ($value) {
                $query->where('cities.name', 'ilike', '%'.$value.'%')
                    ->orWhere('cities.code', 'ilike', '%'.$value.'%')
                    ->orWhere('cities.dane_code', 'ilike', '%'.$value.'%');
            })
            ->where('cities.active', '=', true)
            ->limit(20)
            ->get();
    }

    /**
     * @param string $value
     * @return Collection
     */
    public function search(string $value):Collection
    {
        $this->baseSelectQuery();

        return $this->query
            ->where('cities.name', 'ilike', '%'.$value.'%')
            ->orWhere('cities.code', 'ilike', '%'.$value.'%')
            ->orWhere('cities.dane_code', 'ilike', '%'.$value.'%')
            ->where('cities.active', '=', true)
            ->limit(20)
            ->get();
    }
}
