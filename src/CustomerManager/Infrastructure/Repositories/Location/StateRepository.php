<?php

namespace Customer\Infrastructure\Repositories\Location;

use Customer\Infrastructure\Interfaces\Repositories\Location\StateRepositoryInterface;
use Customer\Infrastructure\Repositories\BaseRepository;
use Illuminate\Support\Collection;

class StateRepository extends BaseRepository implements StateRepositoryInterface
{
    /**
     * @var string
     */
    protected string $databaseConnection = 'pgsql';

    /**
     * @var string
     */
    protected string $tableName = 'general.states';

    /**
     * @param string $value
     * @return Collection
     */
    public function search(string $value):Collection
    {
        return $this->query
            ->select([
                'states.id AS id',
                'states.name AS name',
                'states.code AS code',
                'states.dane_code AS dane_code',
                'countries.id AS country_id',
                'countries.name AS country_name',
                'countries.iso_code AS country_iso_code',
            ])
            ->leftJoin('general.countries', 'countries.id', '=', 'states.country_id')
            ->where('states.name', 'ilike', '%'.$value.'%')
            ->orWhere('states.code', 'ilike', '%'.$value.'%')
            ->orWhere('states.dane_code', 'ilike', '%'.$value.'%')
            ->where('states.active', '=', true)
            ->limit(20)
            ->get();
    }
}
